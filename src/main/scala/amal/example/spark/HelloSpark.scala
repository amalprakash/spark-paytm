/**
 * README : Please pass the below VM arguments before running:
 *            -Dlog4j.configuration=file:log4j.properties -Dspark.yarn.app.container.log.dir=app-log -Dlogfile.name=my-log
 *
 * 
 * */


package amal.example.spark

import org.apache.log4j.Logger
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.udf
import org.apache.spark.sql.types.{DateType, IntegerType, StringType, StructField, StructType}

object HelloSpark {

  @transient lazy val logger:Logger = Logger.getLogger(getClass.getName)
  def main(args: Array[String]):Unit = {
    val spark = SparkSession.builder()
      .appName("MyPayTMApp")
      .master("local[*]")
      .getOrCreate()

    val weatherSchema = StructType(Array(StructField("STN---",StringType,true),
      StructField("wban",StringType,true),
      StructField("yearmoda",StringType,true),
      StructField("TEMP",StringType,true),
      StructField("dewp",StringType,true),
      StructField("slp",StringType,true),
      StructField("stp",StringType,true),
      StructField("visib",StringType,true),
      StructField("wdsp",StringType,true),
      StructField("mxspd",StringType,true),
      StructField("gust",StringType,true),
      StructField("max",StringType,true),
      StructField("min",StringType,true),
      StructField("prcp",StringType,true),
      StructField("sndp",StringType,true),
      StructField("frshtt",StringType,true)
    ))

    //Define UDFs to clean values
    val tempCleanUDF = udf(cleanTemp(_:String):String)
    val windCleanUDF = udf(cleanWindSpeed(_:String):String)

    //Read data as Dataframes
    val weatherData = spark.read.schema(weatherSchema).option("header","true").csv("data/2019/*.csv.gz")
    val countryData = spark.read.option("header","true").option("inferSchema","true").csv("data/countrylist.csv")
    val stationList =  spark.read.option("header","true").option("inferSchema","true").csv("data/stationlist.csv")


    val joinExpr1 = weatherData.col("STN---") === stationList.col("STN_NO")
    val countryCodeJoinedDF = weatherData.join(stationList, joinExpr1, "left")


    val joinExpr2 = countryCodeJoinedDF.col("COUNTRY_ABBR") === countryData.col("COUNTRY_ABBR")
    val joinedRawWeatherData = countryCodeJoinedDF.join(countryData, joinExpr2, "left")

    import org.apache.spark.sql.functions._

    //order countries by avg temperature and take first record
    val hottestCountry = joinedRawWeatherData.groupBy("COUNTRY_FULL").agg(avg(cleanTemp("temp")).as("avg_yearly_temp"))
      .orderBy(desc("avg_yearly_temp"))
      .select("COUNTRY_FULL","avg_yearly_temp").first().mkString("-->")

    println("\n\n\nHottest country and avg. temperature:"+hottestCountry)

    //order countries by avg wind speed, collect as array and take index 1
    val windSpeedDF = joinedRawWeatherData.groupBy("COUNTRY_FULL").agg(avg(cleanWindSpeed("WDSP")).as("avg_wind_speed"))
      .orderBy(desc("avg_wind_speed"))
      .select("COUNTRY_FULL","avg_wind_speed").limit(2)

      println("Country with second highest avg. wind speed: "+windSpeedDF.rdd.zipWithIndex().collect()(1)._1.mkString("-->"))


    spark.stop()
  }

  def cleanTemp(temp:String):String = {
    if (temp.replace("*","").equals("9999.9"))
      ""
      else
      temp.replace("*","")
  }

  def cleanWindSpeed(wind:String):String = {
    if (wind.replace("*","").equals("999.9"))
      ""
    else
      wind.replace("*","")
  }

}
